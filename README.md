This repo contains some files that demonstrate some results of the Screenomics Fact project.

- poster.pdf: introduction to the project and some visualization
- project.ipynb: cnn training and predictions with screenshot data
- MergeTag.R: Preprocessing of text and ground truth data
- FactClassification.R: ML experiments besides cnn
- FactScoreDistributionAndAnalysis.pdf: Statistical analysis of factual perception

